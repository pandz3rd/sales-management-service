package com.pandz.salesmanagement.util;

import com.google.gson.*;

import java.util.Random;

public class Utilities {
//  public static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
  public static final Gson GSON = (new GsonBuilder()).create();

  private Utilities() {
  }

  public static <E> E fromJson(String json, Class<E> clazz) {
    Object ret = null;

    try {
      if (null == json) {
        throw new JsonParseException("Null json string");
      }

      String _json = json.trim();
      if (_json.length() < 1) {
        throw new JsonParseException("Zero length json string");
      }

      if (_json.charAt(0) != '{' || !_json.endsWith("}")) {
        throw new JsonParseException("Invalid json format");
      }

      ret = GSON.fromJson(json, clazz);
    } catch (Throwable var4) {
      System.out.println("### Error parse from json : " + var4);
    }

    return (E) ret;
  }

  public static String toJson(Object object) {
    String ret = (String)null;

    try {
      ret = GSON.toJson(object);
    } catch (Throwable var3) {
      System.out.println("### Error parse to json : " + var3);
    }

    return ret;
  }

  public static JsonArray stringToJsonArray(String json) {
    JsonArray jsonArr = (JsonArray)null;

    try {
      JsonParser parser = new JsonParser();
      JsonElement jsonElement = parser.parse(json);
      jsonArr = jsonElement.getAsJsonArray();
    } catch (Throwable var4) {
      System.out.println("### Error parse to jsonArray : " + var4);
    }

    return jsonArr;
  }

  public static String generateInvoice() {
    Random rnd = new Random();
    int number = rnd.nextInt(999999);

    StringBuilder invoice = new StringBuilder("TR00"+number);

    return invoice.toString();
  }
}
