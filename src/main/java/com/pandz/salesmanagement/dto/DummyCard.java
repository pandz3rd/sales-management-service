package com.pandz.salesmanagement.dto;

public class DummyCard {
  public static AccountCard getDummyCard1() {
    AccountCard accountCard = new AccountCard();
    accountCard.setAccountName("Gold");
    accountCard.setAccountNumber("5481160000000022");
    accountCard.setAccountProductType("SaxTra");
    accountCard.setAccountStatus("1");
    accountCard.setAccountType("30");
    accountCard.setCardNumber("5481160000000123");
    accountCard.setCardStatusCode("1");
    accountCard.setCurrency("IDR");
    accountCard.setFlag("D");
    accountCard.setCustomerName("Cah Kerjo");
    accountCard.setProductCode("XXX");

    return accountCard;
  }

  public static AccountCard getDummyCard2() {
    AccountCard accountCard = new AccountCard();
    accountCard.setAccountName("Silver");
    accountCard.setAccountNumber("5481160000000022");
    accountCard.setAccountProductType("SaxTra");
    accountCard.setAccountStatus("1");
    accountCard.setAccountType("SaxTra|tr");
    accountCard.setCardNumber("5481160000000124");
    accountCard.setCardStatusCode("1");
    accountCard.setCurrency("IDR");
    accountCard.setFlag("D");
    accountCard.setCustomerName("Aku cah kerja");
    accountCard.setProductCode("XXX");

    return accountCard;
  }

  public static AccountCard getDummyCard3() {
    AccountCard accountCard = new AccountCard();
    accountCard.setAccountName("Silver");
    accountCard.setAccountNumber("5481160000000023");
    accountCard.setAccountProductType("SaxTra");
    accountCard.setAccountStatus("1");
    accountCard.setAccountType("SaxTra|tr");
    accountCard.setCardNumber("5481160000000125");
    accountCard.setCardStatusCode("1");
    accountCard.setCurrency("IDR");
    accountCard.setFlag("D");
    accountCard.setCustomerName("Whitemon");
    accountCard.setProductCode("XXX");

    return accountCard;
  }
}
