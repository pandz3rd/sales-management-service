package com.pandz.salesmanagement.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AccountCard {
  private String accountNumber;
  private String accountType;
  private String currency;
  private String accountName;
  private String sofFlag;
  private String balance;
  private String productCode;
  private String productName;
  private String accountStatus;
  private String accountVisibility;
  private String quickBalance;
  private String branchCode;
  private String cardNumber;
  private String accountProductType;
  private String cardStatusCode;
  private String expiredDate;
  private String customerName;
  private String isPrimary;
  private String linkageStatus;
  private String flag;
}
