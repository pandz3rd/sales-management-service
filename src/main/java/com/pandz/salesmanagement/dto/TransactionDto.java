package com.pandz.salesmanagement.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransactionDto {
  private Integer id;
  private String name;
  private Integer price;
  private Integer stock;
  private Integer total;

  private Integer amount;

  private Integer salesId;
  private String customerName;
  private String address;
  private String email;
  private String statusSales;
  private String invoiceNumber;
  private Date transactionDate;

}
