package com.pandz.salesmanagement.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InquiryRq {
  private Integer stuffId;
  private Integer amount;
  private String customerName;
  private String email;
  private String address;
}
