package com.pandz.salesmanagement.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SalesDetailDto {
  private Integer id;
  private Integer t_sales_id;
  private Integer m_stuff_id;
  private Integer total_stuff;
}
