package com.pandz.salesmanagement.messagesender;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pandz.salesmanagement.constant.GlobalConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class MessageSender {
  private static final Logger log = LoggerFactory.getLogger(MessageSender.class);

  @Autowired
  private RabbitTemplate rabbitTemplate;

  public void sendEmail(Map<String, String> req) {
    log.info("Send message info: " + req);
    try {
      ObjectMapper obj = new ObjectMapper();
      rabbitTemplate.convertAndSend(GlobalConstant.NOTIF_PAYMENT, obj.writeValueAsString(req));
    } catch (JsonProcessingException e) {
      e.printStackTrace();
      log.info("Error json: " + e);
    }
  }
}
