package com.pandz.salesmanagement.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pandz.salesmanagement.constant.GlobalConstant;
import com.pandz.salesmanagement.dto.*;
import com.pandz.salesmanagement.entity.Sales;
import com.pandz.salesmanagement.messagesender.MessageSender;
import com.pandz.salesmanagement.repository.SalesRepository;
import com.pandz.salesmanagement.util.Utilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class SalesImpl {
  private static final Logger log = LoggerFactory.getLogger(SalesImpl.class);

  private MessageSender messageSender;
  private SalesRepository salesRepository;

  @Autowired
  public SalesImpl(MessageSender messageSender, SalesRepository salesRepository) {
    this.messageSender = messageSender;
    this.salesRepository = salesRepository;
  }

  public ResponseStatus findByName(String name) {
    log.info("Start Find Sales by Name");
    log.info("Name: " + name);
    Sales res = salesRepository.findByName(name);

    if (res != null) {
      return ResponseStatus.builder()
          .code("00")
          .message("SUCCESS")
          .description("Get Sales successfully")
          .response(res)
          .build();
    } else {
      return ResponseStatus.builder()
          .code("01")
          .message("FAILED")
          .description("Data not found")
          .build();
    }
  }

  public ResponseStatus addSale(Sales req) {
    log.info("Start Save Sales");

    Sales res = new Sales();
    Sales salesExisting = new Sales();
    Date date = new Date();
    Integer id = req.getId();

    String invoiceNumber = Utilities.generateInvoice();

    // Find existing sales
    if (id != null) {
      salesExisting = salesRepository.searchById(req.getId());
      salesExisting.setId(req.getId());
    }

    salesExisting.setAddress(req.getAddress());
    salesExisting.setCustomer_name(req.getCustomer_name());
    salesExisting.setEmail(req.getEmail());
    salesExisting.setStatus(req.getStatus());
    salesExisting.setInvoice_number(invoiceNumber);
    salesExisting.setTransaction_date(date);

    res = salesRepository.save(salesExisting);

    if (res != null) {
      return ResponseStatus.builder()
          .code("00")
          .message("SUCCESS")
          .description("Sales save successfully")
          .build();
    } else {
      return ResponseStatus.builder()
          .code("01")
          .message("FAILED")
          .description("Failed save sales")
          .build();
    }
  }

  public ResponseStatus showAllSales() {
    log.info("Start find all Sales");

    List<Sales> res = salesRepository.findAll();

    if (res != null) {
      return ResponseStatus.builder()
          .code("00")
          .message("SUCCESS")
          .description("All sales")
          .response(res)
          .build();
    } else {
      return ResponseStatus.builder()
          .code("01")
          .message("SUCCESS")
          .description("No sales found")
          .build();
    }
  }

  public ResponseStatus inquiry(InquiryRq req) {
    log.info("Start Inquiry");

    String resCode = "00";
    String resStatus = "SUCCESS";
    String resDescription = "Success inquiry";
    Integer totalPrice = 0;

    RestTemplate restTemplate = new RestTemplate();
    TransactionDto res = new TransactionDto();
    TransactionDto reqStuff = new TransactionDto();

    reqStuff.setId(req.getStuffId());

    ResponseStatus resStuff = restTemplate.postForObject(GlobalConstant.STUFF, reqStuff, ResponseStatus.class);
    System.out.println("Res: " + resStuff);

    if (resStuff.getCode().equals("00")) {
      Object obj = resStuff.getResponse();
      res = Utilities.fromJson(obj.toString(), TransactionDto.class);

      Integer stock = res.getStock();
      Integer price = res.getPrice();

      if (stock < req.getAmount()) {
        resCode = "01";
        resStatus = "FAILED";
        resDescription = "Out of stock";
      } else {
        totalPrice = price * req.getAmount();
        res.setTotal(totalPrice);

        // Post sales status pending
        Date date = new Date();
        String invoiceNumber = Utilities.generateInvoice();
        String statusSales = "PENDING";

        Sales reqSales = new Sales();
        reqSales.setCustomer_name(req.getCustomerName());
        reqSales.setEmail(req.getEmail());
        reqSales.setAddress(req.getAddress());
        reqSales.setStatus(statusSales);
        reqSales.setTransaction_date(date);
        reqSales.setInvoice_number(invoiceNumber);

        salesRepository.save(reqSales);

        // Add sales info into response
        Sales salesInserted = salesRepository.searchByInvoice(invoiceNumber);
        res.setSalesId(salesInserted.getId());
        res.setCustomerName(salesInserted.getCustomer_name());
        res.setAddress(salesInserted.getAddress());
        res.setEmail(salesInserted.getEmail());
        res.setStatusSales(salesInserted.getStatus());
        res.setInvoiceNumber(salesInserted.getInvoice_number());
        res.setTransactionDate(salesInserted.getTransaction_date());

      }
    } else {
      resCode = "01";
      resStatus = "FAILED";
      resDescription = "Failed get stuff data";
    }

    return ResponseStatus.builder()
        .code(resCode)
        .message(resStatus)
        .description(resDescription)
        .response(res)
        .build();
  }

  public ResponseStatus payment(TransactionDto req) {
    log.info("Start Payment");

    String resCode = "00";
    String resStatus = "SUCCESS";
    String resDesc = "Transaction Successfully";

    RestTemplate restTemplate = new RestTemplate();

    String statusSales = "SUCCESS";
    Date tranDate = new Date();

    // Update status sales
    Sales reqSales = new Sales();
    reqSales.setId(req.getSalesId());
    reqSales.setCustomer_name(req.getCustomerName());
    reqSales.setStatus(statusSales);
    reqSales.setAddress(req.getAddress());
    reqSales.setEmail(req.getEmail());
    reqSales.setInvoice_number(req.getInvoiceNumber());
    reqSales.setTransaction_date(tranDate);

    Sales resSales = salesRepository.save(reqSales);
    System.out.println("Res sales: " + resSales);

    // update stock stuff
    TransactionDto reqStuff = new TransactionDto();
    reqStuff.setId(req.getId());
    reqStuff.setPrice(req.getPrice());
    reqStuff.setStock(req.getStock() - req.getAmount());
    reqStuff.setName(req.getName());
    ResponseStatus resStuff = restTemplate.postForObject(GlobalConstant.STUFF_SAVE, reqStuff, ResponseStatus.class);

    System.out.println("Res update stuff: " + resStuff);

    // Insert sales detail
    SalesDetailDto reqSalesDetail = new SalesDetailDto();
    reqSalesDetail.setT_sales_id(req.getSalesId());
    reqSalesDetail.setM_stuff_id(req.getId());
    reqSalesDetail.setTotal_stuff(req.getPrice() * req.getAmount());
    ResponseStatus resSalesDetail = restTemplate.postForObject(GlobalConstant.SALES_DETAIL_SAVE, reqSalesDetail, ResponseStatus.class);

    System.out.println("Res insert sales detail: " + resSalesDetail);

    if (resSales == null || resStuff == null || resSalesDetail == null) {
      resCode = "01";
      resStatus = "FAILED";
      resDesc = "Failed payment";
    }

    // Post to message broker to trigger notif email
    String descEmail = "Payment success untuk barang: " + req.getName() +
        ". \nJumlah barang: " + req.getAmount() +
        ". \nHarga barang: " + req.getPrice() +
        ". \nTotal harga: " + req.getPrice() * req.getAmount() +
        ". \nInvoice number: " + req.getInvoiceNumber();

    Map<String, String> emailInfo = new HashMap<>();
    emailInfo.put("from", "topanardianto@gmail.com");
    emailInfo.put("to", "topanardianto@gmail.com");
    emailInfo.put("subject", "Payment Success");
    emailInfo.put("message", descEmail);
    messageSender.sendEmail(emailInfo);

    // TODO: response sales detail

    return ResponseStatus.builder()
        .code(resCode)
        .message(resStatus)
        .description(resDesc)
        .build();
  }

  // Learn stream syntax java
  public static void main(String[] args) {
    // get dummies card
    List<AccountCard> listCard = new ArrayList<AccountCard>();
    listCard.add(DummyCard.getDummyCard1());
    listCard.add(DummyCard.getDummyCard2());
    listCard.add(DummyCard.getDummyCard3());

    // filter by card number
    List<AccountCard> resultCard = listCard.stream().filter(c -> c.getAccountNumber().equalsIgnoreCase("5481160000000022"))
      .collect(Collectors.toList());
    System.out.println("\nFilter card number 5481160000000022: ");
    resultCard.forEach(System.out::println);

    // Filter list string
    List<String> carList = Arrays.asList("Toyota", "Mitsubisi", "Fortuner");
    List<String> resultCar = carList.stream()
        .filter(c -> !"toyota".equalsIgnoreCase(c))
        .collect(Collectors.toList());
    System.out.println("\nFilter car not Toyota: ");
    resultCar.forEach(System.out::println);

    // Filter card by customer name
    AccountCard resultCustName = listCard.stream()
        .filter(c -> "whitemon".equalsIgnoreCase(c.getCustomerName()) && "IDR".equalsIgnoreCase(c.getCurrency()))
        .findAny()
        .orElse(null);
    System.out.println("\nFilter card by customer name: " + resultCustName);

    // Filter card by customer name, then convert to string
    String accNumber = listCard.stream()
        .filter(c -> "whitemon".equalsIgnoreCase(c.getCustomerName()))
        .map(AccountCard::getAccountNumber) // convert to string, get account number
        .findAny()
        .orElse(null);
    System.out.println("\nFilter card by customer name: " + accNumber); // 5481160000000023

    List<String> listName = listCard.stream()
        .map(AccountCard::getCustomerName)
        .collect(Collectors.toList());
    System.out.println("\nMap get customer name only: "); // Cah Kerjo, Aku cah kerja, Whitemon
    listName.forEach(System.out::println);

    // Filter sorted
    List<String> sortedName = listCard.stream()
//        .filter(n -> "Cah Kerjo".equalsIgnoreCase(n.getCustomerName()))
        .map(AccountCard::getCustomerName)
        .sorted()
        .collect(Collectors.toList());
    System.out.println("\nFilter sorted: "); // Aku cah kerja, Cah Kerjo, Whitemon
    sortedName.forEach(System.out::println);

    // Filter integer limit
    List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
    List<Integer> resultLimit = numbers.stream()
        .filter(n -> {
//          System.out.println("\nFiltering: " + n);
          return n % 2 == 0;
        })
        .map(n -> {
//          System.out.println("Mapping: " + n);
          return n * n;
        })
        .limit(2)
        .collect(Collectors.toList());
    System.out.println("\nResult filter limit: " + resultLimit); // [4, 16]

    // Filter integer result boolean
    boolean isRemidi = numbers.stream()
        .anyMatch(t -> 11 == t);
    System.out.println("\nAll match: " + isRemidi); // false

    // Filter get length string
    List<Integer> lengthList = carList.stream()
        .map(String::length)
        .collect(Collectors.toList());
    System.out.println("\nResult length string: " + lengthList); // [6, 9, 8]

    // Reduce
    int reduceNumber = numbers.stream()
        .reduce(0, (a, b) -> a + b);
    System.out.println("\nResult reduce: " + reduceNumber); // 36

    int maxReduceNumber = numbers.stream()
        .reduce(0, Integer::max);
    System.out.println("\nResult max reduce: " + maxReduceNumber); // 8

    // Stream iterate
    Stream<Integer> integerStream = Stream.iterate(0, n -> n + 10);
    System.out.println("\nStream iterate: ");
    integerStream.limit(5).forEach(System.out::println); // 0, 10, 20, 30, 40

  }
}
