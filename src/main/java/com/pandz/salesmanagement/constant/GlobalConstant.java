package com.pandz.salesmanagement.constant;

public class GlobalConstant {
  // PATH
  public static final String SALES = "/sales";
  public static final String INSERT_SALES = SALES + "/save";
  public static final String ALL_SALES = SALES + "/all";

  public static final String INQUIRY = "/inquiry";
  public static final String PAYMENT = "/payment";

  // URL
  public static final String STUFF_ALL = "http://localhost:8081/stuff/all";
  public static final String STUFF = "http://localhost:8081/stuff";
  public static final String STUFF_SAVE = "http://localhost:8081/stuff/save";

  public static final String SALES_DETAIL = "http://localhost:8081/salesdetail";
  public static final String SALES_DETAIL_SAVE = SALES_DETAIL + "/save";

  // Variable
  public static final String NOTIF_PAYMENT = "notification-payment";
}
