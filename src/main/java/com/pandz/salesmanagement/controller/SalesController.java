package com.pandz.salesmanagement.controller;

import com.pandz.salesmanagement.dto.InquiryRq;
import com.pandz.salesmanagement.dto.ResponseStatus;
import com.pandz.salesmanagement.dto.TransactionDto;
import com.pandz.salesmanagement.entity.Sales;
import com.pandz.salesmanagement.repository.SalesRepository;
import com.pandz.salesmanagement.service.SalesImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static com.pandz.salesmanagement.constant.GlobalConstant.*;

@RestController
public class SalesController {
  private com.pandz.salesmanagement.repository.SalesRepository salesRepo;

  @Autowired
  public SalesController(SalesRepository salesRepository) {
    this.salesRepo = salesRepository;
  }

  @Autowired
  private SalesImpl sales;

  @GetMapping(SALES)
  public ResponseStatus getSales(@RequestBody Sales payload) {
    String name = payload.getCustomer_name();
    System.out.println("Customer name: " + name);

    if (name.isEmpty()) {
      return ResponseStatus.builder()
          .code("01")
          .message("BAD REQUEST")
          .description("Field customer name is required")
          .build();
    }

    return sales.findByName(name);
  }

  @PostMapping(INSERT_SALES)
  public ResponseStatus insertSale(@RequestBody Sales req) {
    return sales.addSale(req);
  }

  @GetMapping(ALL_SALES)
  public ResponseStatus getAllSales() {
    return sales.showAllSales();
  }

  @PostMapping(INQUIRY)
  @Transactional
  public ResponseStatus inquiry(@RequestBody InquiryRq req) {
    return sales.inquiry(req);
  }

  @PostMapping(PAYMENT)
  @Transactional
  public ResponseStatus payment(@RequestBody TransactionDto req) {
    return sales.payment(req);
  }
}
