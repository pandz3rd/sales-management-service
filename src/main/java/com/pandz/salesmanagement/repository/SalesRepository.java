package com.pandz.salesmanagement.repository;

import com.pandz.salesmanagement.entity.Sales;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SalesRepository extends JpaRepository<Sales, String> {
  @Query(value = "SELECT * FROM t_sales WHERE customer_name = ?1", nativeQuery = true)
  Sales findByName(String name);

  List<Sales> findAll();

  @Query(value = "SELECT * FROM t_sales WHERE id = ?1", nativeQuery = true)
  Sales searchById(int id);

  @Query(value = "SELECT * FROM t_sales WHERE invoice_number = ?1", nativeQuery = true)
  Sales searchByInvoice(String invoiceNumber);
}
