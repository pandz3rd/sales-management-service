package com.pandz.salesmanagement.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "t_sales")
public class Sales {

  @Id
  @GeneratedValue(generator = "UUID")
  @Column(name = "id", updatable = false)
  private Integer id;
  private String customer_name;
  private String address;
  private String status;
  private String email;
  private String invoice_number;
  private Date transaction_date;

  public Sales() {}

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getCustomer_name() {
    return customer_name;
  }

  public void setCustomer_name(String customer_name) {
    this.customer_name = customer_name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getInvoice_number() {
    return invoice_number;
  }

  public void setInvoice_number(String invoice_number) {
    this.invoice_number = invoice_number;
  }

  public Date getTransaction_date() {
    return transaction_date;
  }

  public void setTransaction_date(Date transaction_date) {
    this.transaction_date = transaction_date;
  }
}
